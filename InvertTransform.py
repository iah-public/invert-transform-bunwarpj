# InvertTransform.py
# Author: Son Phan, IAH Pasteur
#
# This script perform invert transformation of an image that previously transformed by bUnwarpJ.
# Note that the script is very specific for the project where the apical surfaces are segmented and tracked via TissueAnalyzer.
# TissueAnalyzer export the labeled apical surfaces in RGB values.
#
# Here is what we do:
# 1. Convert RGB value to ID number corresponding to the column "local_id_cells" in exported cells.csv from TissueAnalyzer.
# 2. Apply invert transform on these segmented surfaces and compute the original apical areas.
# 3. Put the original apical areas to the cells.csv
#
# Remark: this script works only for "outi", "moji", "kati". For "pomi" please run InvertTransformV2.py
# 
# Parameters:
# 1. dataname: dataset name, would be "outi", "moji" or "kati".
# 2. base_trans_dir: directory contains the bUnwarpJ-transformed text files.
# 3. base_img_dir: directory contains RGB-labels of segmented surfaces from TissueAnalyzer.
# 4. base_out_dir: directory to save the invert-transformed labels.

import os
import glob

from ij import IJ
from ij import ImagePlus
from ij.plugin import ChannelSplitter, Thresholder

from inra.ijpb.plugins import NeighborLabelsPlugin
from inra.ijpb.binary import BinaryImages
from bunwarpj import bUnwarpJ_

IJ.run("Close All")

dataname = "kati"

base_trans_dir = "/media/Data/Perso/Phan/IAH/NDray/InvertTransformation/Data/tissue_analyzer/kati/kati_bunwarpj/kati_ref10_refine"
trans_lst = glob.glob(os.path.join(base_trans_dir,"*.txt"))

# There is only reference frame missed, so last frame number = nb of files + 1
lastframe = len(trans_lst) + 1 

str1 = os.path.basename(trans_lst[0]).split("ref")[1]
str2 = str1.split("_frame")[0]
refframe = int(str2)
print "Reference frame =",refframe

base_img_dir = "/media/Data/Perso/Phan/IAH/NDray/InvertTransformation/Data/tissue_analyzer/kati/kati_tissueanalyzer/SEG"
base_out_dir = "/media/Data/Perso/Phan/IAH/NDray/InvertTransformation/Output/{}".format(dataname)

for frame in range(1,lastframe+1):
#for frame in [11]:

	if frame == refframe:
		continue

	print "Process frame {:02d}...".format(frame)
	input_img = os.path.join(base_img_dir,"Registered_subback_kati-merged_{:02d}/cell_identity.tif".format(frame))
	trans_txt = os.path.join(base_trans_dir,"ref{}_frame{}.txt".format(refframe,frame))
	rtrans_txt = os.path.join(base_out_dir,"ref{}_frame{}_raw.txt".format(refframe,frame))
	itrans_txt = os.path.join(base_out_dir,"ref{}_frame{}_raw_invert.txt".format(refframe,frame))
	
	# Open input movie, select blue channel
	try:
		img = IJ.openImage(input_img)
		imgs = ChannelSplitter().split(img)
		blueChannel = imgs[2]
		blueChannel.show()
	except:
		# Can not find cell_identity.tif is some frames. Don't know why.
		print "Can not read image of frame {:02d}".format(frame)
		continue
	
	# Convert elastic to raw
	bUnwarpJ_().convertToRaw(trans_txt,rtrans_txt,blueChannel.getTitle())
	
	# Invert transform
	bUnwarpJ_().invertRawTransform(rtrans_txt,itrans_txt,blueChannel.getTitle())
	
	# Extract a label
	for labelID in range(1,255):
#	for labelID in [2]:
		
		labelImg = NeighborLabelsPlugin().selectedLabelsToMask(blueChannel,[labelID])
#		labelImg.show()
		
		# Apply invert transformation to label image
		bUnwarpJ_().applyRawTransformToSource(itrans_txt, labelImg, labelImg)
		
		# Create mask
		flag = True
		try:
			IJ.setRawThreshold(labelImg,100,10000)
			mask = labelImg.createThresholdMask()
			newLabelImg = BinaryImages.componentsLabeling(mask,4,16)
			newLabelImg = BinaryImages.keepLargestRegion(newLabelImg)
		except:
			flag = False
			print "Can not create mask for inverted cell"
		
		if flag == True:
			filename = "ref{}_frame{}_invert_{}".format(refframe,frame,labelID)
			newLabelImg = ImagePlus(filename,newLabelImg)
#			newLabelImg.show()
			filepath = os.path.join(base_out_dir,filename)
			IJ.saveAs(newLabelImg,"Tiff",filepath)
			
print("Done")






