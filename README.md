# Invert Transform bUnwarpJ

Movie of apical surfaces was stabilized by bUnwarpJ, then the segmentation and tracking were done by TissueAnalyzer.
This repo provides scripts for computing the original apical areas before using the bUnwarpJ.

## Invert Transform

We provide two scripts that can be run from Fiji to do the invert transformation of labels of segmented cells from TissueAnalyzer.

- Run InvertTransform.py for outi, moji or kati dataset.
- Run InvertTransformV2.py for pomi dataset.

## Compute apical areas

Run ProcessInvertArea.ipynb to measure the areas of inverted cells. The computed areas are then added to cells.csv.
